require("dotenv").config();

const express = require("express");
const app = express();
const proxyApp = express();
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const formidable = require("formidable");
const fs = require("fs");
const nodemailer = require("nodemailer");
const https = require("https");
const http = require('http');
const axios = require('axios');

let httpsoptions;
const env = process.env.NODE_ENV || 'development';
/*console.log('environment es', env);
console.log(process.env.NODE_ENV);
  if((process.env.NODE_ENV || '').trim() === 'development') {
  console.log('env', env)
  httpsoptions = {
    cert: fs.readFileSync(path.join(__dirname, "localhostCerts", "./certificate.crt")),
    key: fs.readFileSync(path.join(__dirname, "localhostCerts", "./certificate.key")),
  };
} 
if ((process.env.NODE_ENV || '').trim() === 'production') {
  console.log('prod', env);
  httpsoptions = {
    cert: fs.readFileSync(path.join(__dirname, "certs", "./certificate.pem")),
    key: fs.readFileSync(path.join(__dirname, "certs", "./private.key")),
    ca:  [fs.readFileSync(path.join(__dirname, "certs", "./ca_bundle.pem")) ],
    checkServerIdentity: () => { return null; }
  };
}*/
   

//production certs, only use in prod
httpsoptions = {
  cert: fs.readFileSync(path.join(__dirname, "certs", "./certificate.pem")),
  key: fs.readFileSync(path.join(__dirname, "certs", "./private.key")),
  ca:  [fs.readFileSync(path.join(__dirname, "certs", "./ca_bundle.pem")) ],
  checkServerIdentity: () => { return null; }
};
app.enable('trust proxy')

app.use((req, res, next) => {
  req.secure ? next() : res.redirect('https://' + req.headers.host + req.url)
})
app.use(cors());
const publicPath = path.join(__dirname, "/build");
//database
const JsonDB = require("node-json-db");
const Config = require("node-json-db/dist/lib/JsonDBConfig");
const db = new JsonDB.JsonDB(
  new Config.Config("transportadoradatabase", true, false, "/")
);
//revisar si la base de datos tiene la propiedad comisiones, si no es asi es la primera vez
//que se carga y debe agregarse, de lo contrario ignorar
const data = db.getData("/");
if (!data.comisiones) {
  db.push(
    "/comisiones",
    {
      envia: {
        fletePorKilo: 2600,
        porcentajeRecaudo: 1,
      },
      tcc: {
        fletePorKilo: 2600,
        porcentajeRecaudo: 1,
      },
      mensajerosUrbanos: {
        fletePorKilo: 2600,
        porcentajeRecaudo: 1,
      },
    },
    false
  );
}

app.use(express.static(publicPath));
app.use("*", express.static(publicPath + "/index.html"));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ extended: true }));
/* service: 'gmail',*/
const transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 465,
  auth: {
    user: process.env.HORENVIOS_EMAL_USER,
    pass: process.env.HORENVIOS_EMAIL_KEY,
  },
});
app.post("/registro", (req, res) => {
  //primero revisar que la base de datos exista
  let data;
  try {
    data = db.getData("/");
  } catch (error) {
    // The error will tell you where the DataPath stopped.
    //la base de datos no existe
    const message = "La base de datos no existe";
    res.status(400).send(message);
  }
  //data = db.getData("/");
  if (data !== undefined) {
    //la base de datos existe, revisar que el correo no exista en la base de datos
    const userInfoKeys = Object.keys(data);
    let horCodeNumber;
    if (userInfoKeys.includes(req.body.Email)) {
      const message =
        "El correo ya existe en la base de datos, es necesario registrarse con uno nuevo";
      res.status(409).send(message);
    } else {
      //el correo no esta en la base de datos, se puede guardar y crear un nuevo usuario
      //analizar primero que código se debe añadir (HOR1050, HOR1051)
      let body = req.body;
      const codigoSuper = req.body.CodigoSuperUsuario;
      //analizar si se trata de un super usuario, si es asi revisar que el código sea correcto
      if (codigoSuper !== undefined && codigoSuper === "Horenvios2020") {
        //el codigo es correcto
        if (userInfoKeys.length === 0 || userInfoKeys.length === 1 ) {
          console.log("124", userInfoKeys);
          //si es el primer usuario en la base de datos el codigo debe ser HOR1051
          horCodeNumber = 1050;
          body.codeNumber = horCodeNumber;
          body.HORCODE = "HOR" + horCodeNumber;
          db.push(
            "/" + req.body.Email,
            {
              ...body,
            },
            false
          );
        } else {
          //ya hay usuarios registrados, revisar cual es el ultimo codigo generado
          console.log("138", userInfoKeys);
          const lastUser = db.getData(
            "/" + userInfoKeys[userInfoKeys.length - 1]
          );
          //horCode = userInfoKeys[userInfoKeys.length - 1].horCode + 1;
          body.codeNumber = lastUser.codeNumber + 1;
          body.HORCODE = "HOR" + (lastUser.codeNumber + 1);
          db.push(
            "/" + req.body.Email,
            {
              ...body,
            },
            false
          );
        }
        const message = `Un usuario con el correo ${req.body.Email} ha sido creado.
      Puedes ingresar con el correo y tu contraseña`;
        //enviar correo

        const htmlBody = `<h1>El usuario ${req.body.Email} ha registrado una cuenta de super usuario</h1><p>Las cuentas de super usuario no necesitan ser habilitadas.</p>`;
        const htmlBody2 =
          "<h1>Registro de Cuenta de Súper Usuario Exitosa</h1><p>Ha registrado una cuenta de súper usuario con Horenvios.</p>";

        const mailOptions = {
          from: "notificaciones.horenvios@gmail.com",
          to: "notificaciones.horenvios@gmail.com",
          subject: `El usuario ${req.body.Email} ha registrado una cuenta de súper usuario.`,
          html: htmlBody,
        };
        const mailOptions2 = {
          from: "notificaciones.horenvios@gmail.com",
          to: req.body.Email,
          subject: `Ha registrado una cuenta de súper usuario exitosamente.`,
          html: htmlBody2,
        };

        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            console.log(error);
          }
        });
        transporter.sendMail(mailOptions2, function (error, info) {
          if (error) {
            console.log(error);
          }
        });
        res.status(201).send(message);
      } else if (codigoSuper !== undefined && codigoSuper !== "Horenvios2020") {
        //codigo de super usuario incorrecto
        const message = "El código de super usuario es incorrecto";
        res.status(400).send(message);
      } else if (codigoSuper === undefined) {
        //no se trata de super usuario, registrar usuario normal
        if (userInfoKeys.length === 1) {
          //si es el primero usuario en la base de datos el codigo debe ser HOR1051
          horCodeNumber = 1050;
          body.codeNumber = horCodeNumber;
          body.HORCODE = "HOR" + horCodeNumber;
          db.push(
            "/" + req.body.Email,
            {
              ...body,
            },
            false
          );
        } else {
          //ya hay usuarios registrados, revisar cual es el ultimo codigo generado
          const lastUser = db.getData(
            "/" + userInfoKeys[userInfoKeys.length - 1]
          );
          //horCode = userInfoKeys[userInfoKeys.length - 1].horCode + 1;
          body.codeNumber = lastUser.codeNumber + 1;
          body.HORCODE = "HOR" + (lastUser.codeNumber + 1);
          db.push(
            "/" + req.body.Email,
            {
              ...body,
            },
            false
          );
        }
        const message = `Un usuario con el correo ${req.body.Email} ha sido creado.
      Puedes ingresar con el correo y tu contraseña`;
        //enviar correo
        const htmlBody2 =
          "<h1>Registro de Cuenta Exitosa</h1><p>Ha registrado una cuenta con Horenvios, para habilitarla es necesario subir los documentos en la opción subir documentos de la app.</p>";

        const htmlBody = `<h1>El usuario ${req.body.Email} ha registrado una cuenta</h1><p>Una vez suba los documentos estos llegaran al correo como archivos adjuntos.</p><p>Celular: ${req.body.Celular}</p>`;
        const mailOptions = {
          from: "notificaciones.horenvios@gmail.com",
          to: "notificaciones.horenvios@gmail.com",
          subject: `El usuario ${req.body.Email} ha registrado una cuenta.`,
          html: htmlBody,
        };
        const mailOptions2 = {
          from: "notificaciones.horenvios@gmail.com",
          to: req.body.Email,
          subject: "Ha registrado una cuenta con Horenvios.",
          html: htmlBody2,
        };
        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            console.log(error);
          } else {
          }
        });
        transporter.sendMail(mailOptions2, function (error, info) {
          if (error) {
            console.log(error);
          } else {
          }
        });
        res.status(201).send(message);
      }
    }
  }
});

app.post("/ingresar", (req, res) => {
  //revisar que el correo exista en la base de datos
  const data = db.getData("/");
  const userInfoValues = Object.keys(data);
  if (userInfoValues.includes(req.body.Email)) {
    //el correo si esta en la base de datos, revisar que la contraseña coincida
    const userData = db.getData("/" + req.body.Email);
    if (userData.Password === req.body.Password) {
      //la contraseña coincide
      //verificar si se trata de un super usuario, si es asi enviar toda la info de la base de datos
      if (userData.CodigoSuperUsuario !== undefined) {
        //se trata de un super usuario
        res.status(201).send({ userData, data });
      } else {
        const comisionsOnly = data.comisiones;
        res.status(201).send({ userData, comisionsOnly });
      }
    } else {
      //contraseña incorrecta
      const message = "La contraseña es incorrecta.";
      res.status(400).send(message);
    }

    //
  } else {
    //el correo no esta en la base de datos
    const message = "Correo incorrecto.";
    res.status(400).send(message);
  }
});
app.get("/getInfo", (req, res) => {
  const email = req.body.Email;
  const data = db.getData("/" + email);
  if (data === undefined) {
    //por algun motivo el correo no se encuentra en la base de datos
    res.status(400).send("El correo no se encuentra en la bases de datos");
  } else {
    res.status(200).send(data);
  }
});
app.put("/guardarGuias", (req, res) => {
  const email = req.body.Email;
  //guardar la guia en el correo asociado a la base de datos del usuario
  const data = db.getData("/" + email);
  if (data === undefined) {
    //por algun motivo el correo no se encuentra en la base de datos
    res.status(400).send("El correo no se encuentra en la bases de datos");
  } else {
    //revisar que no se trate de una cancelacion de guia, en ese caso no es necesario agregar
    //nuevos elementos sino actualizar la informacion de la base de datos para que no se dupliquen las guias
    if (req.body.Sobreescribir) {
      //true es override, es decir sobreescribe los datos
      db.push("/" + email + "/GuiasInfo", req.body.GuiasInfo, true);
      db.push(
        "/" + email + "/GuiasInfoRelacion",
        req.body.GuiasInfoRelacion,
        true
      );
    } else {
      //false es merge, es decir mezlca los datos
      db.push("/" + email + "/GuiasInfo", req.body.GuiasInfo, false);
      db.push(
        "/" + email + "/GuiasInfoRelacion",
        req.body.GuiasInfoRelacion,
        false
      );
    }
    res.status(200).send({ userInfo: data, dataInfo: db.data });
  }
});
app.put("/actualizarRelacionGuias", (req, res) => {
  console.log("actualizar relacion guias activated");
  const email = req.body.Email;
  //guardar la guia en el correo asociado a la base de datos del usuario
  const data = db.getData("/" + email);
  if (data === undefined) {
    //por algun motivo el correo no se encuentra en la base de datos
    res.status(400).send("El correo no se encuentra en la bases de datos");
  } else {
    db.push(
      "/" + email + "/GuiasInfoRelacion",
      req.body.GuiasInfoRelacion,
      true
    );
    res.status(200).send({ userInfo: data, dataInfo: db.data });
  }
});
app.put("/agregarAgente", (req, res) => {
  const email = req.body.Email;
  //guardar el agente en el correo asociado a la base de datos del usuario
  const data = db.getData("/" + email);
  if (data === undefined) {
    //por algun motivo el correo no se encuentra en la base de datos
    res.status(400).send("El correo no se encuentra en la bases de datos");
  } else {
    //revisar si hay agentes en el usuario
    try {
      const agentKeys = Object.keys(data.Agentes);
      let body = req.body.Agentes;
      if (agentKeys.length === 0) {
        //si es el primer agente en la base de datos del usuario
        horCodeNumber = 1;
        body.codeNumber = horCodeNumber;
        body.HORCODE = "HOR" + data.codeNumber + "A" + horCodeNumber;
      } else {
        //ya hay agentes registrados, revisar cual es el ultimo numero de agente generado
        const codeNumber = data.Agentes[agentKeys.length - 1].codeNumber;
        body.codeNumber = codeNumber + 1;
        body.HORCODE = "HOR" + data.codeNumber + "A" + body.codeNumber;
      }
      db.push("/" + email + "/Agentes", [body], false);
      res.status(200).send({ userInfoData: data, dataInfo: db });
    } catch (error) {
      res.status(500).send(error.toString());
    }
  }
});
app.post("/enviarDocumentos", (req, res) => {
  const form = new formidable.IncomingForm();
  let userJson = {};
  form.multiples = true;
  form.on("file", function (field, file) {
    if (field === "usuario") {
      fs.readFile(file.path, (err, data) => {
        if (err) {
          console.error(err);
        }
        userJson = JSON.parse(data);
        const dir = "./uploads/" + userJson.Email;
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
          return;
        }
      });
    }
  });
  const paths = [];
  form.parse(req, function (err, fields, files) {
    files.documentos.forEach((el) => {
      const oldpath = el.path;
      const newpath = __dirname + "/uploads/" + userJson.Email + "/" + el.name;
      fs.rename(oldpath, newpath, function (err) {
        if (err) {
          res.send(err);
          throw err;
        }
        //const data = db.getData("/" + userJson.Email);
        //db.push("/" + userJson.Email + "/Data", files.documentos, false);
      });
      paths.push([el.name, newpath]);
    });
    //console.log(paths);
    res.status(200).send("the files uploaded ok");

    //enviar correo

    const attachments = paths.map((doc) => {
      return {
        filename: doc[0],
        path: doc[1],
      };
    });

    const htmlBody = `<h1>El usuario ${userJson.Email} ha subido documentos</h1><p>Revisar los documentos adjuntos.</p>`;
    const mailOptions = {
      from: "notificaciones.horenvios@gmail.com",
      to: "notificaciones.horenvios@gmail.com",
      subject: `El usuario ${userJson.Email} ha subido documentos para habilitar su cuenta.`,
      attachments: attachments,
      html: htmlBody,
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      }
    });
  });
});
app.put("/habilitarDeshabilitarUsuario", (req, res) => {
  const email = req.body.Email;
  const habilitadaDeshabilitada = req.body.CuentaHabilitada;
  //guardar el agente en el correo asociado a la base de datos del usuario
  const data = db.getData("/" + email);
  if (data === undefined) {
    //por algun motivo el correo no se encuentra en la base de datos
    res.status(400).send("El correo no se encuentra en la bases de datos");
  } else {
    //actualizar el estado de cuenta habilitada en el usuario

    db.push("/" + email + "/CuentaHabilitada", habilitadaDeshabilitada, false);
    res.status(200).send(db.data);
  }
});
app.put("/editarComision", (req, res) => {
  const email = req.body.Email;
  //guardar el agente en el correo asociado a la base de datos del usuario
  const data = db.getData("/" + email);
  if (data === undefined) {
    //por algun motivo el correo no se encuentra en la base de datos
    res.status(400).send("El correo no se encuentra en la bases de datos");
  } else {
    //agregar las comisiones nuevas
    try {
      db.push("/comisiones", req.body.Comisiones, false);
      res.status(200).send({ userInfoData: data, dataInfo: db });
    } catch (error) {
      res.status(500).send(error.toString());
    }
  }
});
app.post("/recuperar", (req, res) => {
  //revisar que el correo exista en la base de datos
  const data = db.getData("/");
  const userInfoValues = Object.keys(data);
  if (userInfoValues.includes(req.body.Email)) {
    //el correo si esta en la base de datos, revisar que la contraseña coincida
    const userData = db.getData("/" + req.body.Email);
    const passwordToSend = userData.Password;
    
    //enviar el password al correo
    const htmlBody = `<h1>Ha solicitado recuperar su contraseña en horenvios.com</h1><p>Su contraseña es: ${passwordToSend}</p>`;
        const mailOptions = {
          from: "notificaciones.horenvios@gmail.com",
          to: req.body.Email,
          subject: `Recuperación de contraseña en Horenvios.com.`,
          html: htmlBody,
        };
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        const message = "El correo con su contraseña no se ha podido enviar." + error;
        res.status(400).send(message);
      } else {
        const message = "La contraseña se ha enviado a su correo";
        res.status(201).send(message);
      }
    });
  } else {
    //el correo no esta en la base de datos
    const message = "El correo no existe, por favor revise el correo ingresado.";
    res.status(400).send(message);
  }
});
//si el usuario selecciona todos los usuarios y desea eliminar las guias de varios usuarios al mismo tiempo
//es necesario usar un endpoint diferente que actualize cada usuario independientemente y devuelva los datos
//satisfactoriamente
app.put("/guardarTodasLasGuias", (req, res) => {
  //en el req deben llegar varios emails
  
  //guardar la guia en el correo asociado a la base de datos del usuario
  //en email hay un arreglo que contiene todos los emails, las guiasInfo y la relacion de guias de cada usuario a actualizar
  req.body.DataInfoEntries.forEach((user)=>{
    const data = db.getData("/" + user[1].Email);
    if (data === undefined) {
      //por algun motivo el correo no se encuentra en la base de datos
      res.status(400).send(`El correo ${user[1].Email} no se encuentra en la bases de datos`);
    } else {

      //revisar si se van a cancelar todas las guias, si es asi no hay necesidad de usar filter solo actualizar las guias
      //con la informacion que llega del cliente



      //revisar que no se trate de una cancelacion de guia, en ese caso no es necesario agregar
      //nuevos elementos sino actualizar la informacion de la base de datos para que no se dupliquen las guias
      if (req.body.Sobreescribir) {
        //true es override, es decir sobreescribe los datos
        //agregar condicionalmente las guias especificas del usuario que se esta iterando
        const arregloIdsBodyGuiasInfo = req.body.GuiasInfo.map(guia=>guia.numeroGuia);
        const arregloIdsUserEmail = user[1].GuiasInfo.map(guia=>guia.numeroGuia);
       /* let guiasInfoUsuarioFiltradas = [];
         arregloIdsBodyGuiasInfo.forEach((guiaId, ind)=>{
          if(arregloIdsUserEmail.includes(guiaId)){
            //el arreglo de ids incluye la guia, por lo que hay que actualizarla en la base de datos
            guiasInfoUsuarioFiltradas.push( req.body.GuiasInfo[ind])
          }
        });*/
        //filtrar guiasInfo
        const guiasInfoUsuarioFiltradas = user[1].GuiasInfo.filter((guia)=>{
          //filtrar con las guiasInfoRelacion de la base de datos existente y actualizar con el nuevo estado
          let guiasInfoFiltradas = [];
         data.GuiasInfo.forEach((guias)=>{
            if(guias.numeroGuia === guia.numeroGuia){
              guiasInfoFiltradas.push(guia);
            }
          })
          return guiasInfoFiltradas;
        })

        //hacer lo mismo con guiasInforELACION
        const guiasInfoRelacionUsuarioFiltradas = user[1].GuiasInfoRelacion.filter((guia)=>{
          //filtrar con las guiasInfoRelacion de la base de datos existente y actualizar con el nuevo estado
          let guiasInfoRelacionFiltradas = [];
         data.GuiasInfoRelacion.forEach((guias)=>{
            if(guias.numeroGuia === guia.numeroGuia){
              guiasInfoRelacionFiltradas.push(guia);
            }
          })
          return guiasInfoRelacionFiltradas;
        })
        //console.log('las guias a actualizar son', guiasInfoUsuarioFiltradas.length, guiasInfoRelacionUsuarioFiltradas.length)
        db.push("/" + user[0] + "/GuiasInfo", guiasInfoUsuarioFiltradas, true);
        db.push(
          "/" + user[0] + "/GuiasInfoRelacion",
          guiasInfoRelacionUsuarioFiltradas,
          true
        );
      } else {
        //false es merge, es decir mezlca los datos
        db.push("/" + user[0] + "/GuiasInfo", req.body.GuiasInfo, false);
        db.push(
          "/" + user[0] + "/GuiasInfoRelacion",
          req.body.GuiasInfoRelacion,
          false
        );
      }
      
     
    }
  });
  const email = req.body.Email;
  const userData = db.getData("/" + email);
  res.status(200).send({ userInfo: userData, dataInfo: db.data });
});

const port = process.env.PORT || 443;

https.createServer(httpsoptions, app).listen(port, () => {
  console.log(`listening on port ${port}`);
});
proxyApp.use(cors());
proxyApp.use(bodyParser.json({ limit: "50mb" }));
proxyApp.use(bodyParser.urlencoded({ extended: true }));
proxyApp.post('/proxyParaApiEnviaCotizacion', (req, res)=>{
  
  const cotizacionIp = 'http://200.69.100.66/ServicioLiquidacionREST/Service1.svc/Liquidacion';
  //make post request to envia API
   axios.post(cotizacionIp, req.body)
      .then(function (response) {
        res.json(response.data);
      })
      .catch(function (err) {
        res.status(400).send(err);
      });
  
});

// Redirect from http port 80 to https
http.createServer(proxyApp,function (req, res) {
    res.writeHead(307, { "Location": "https://" + req.headers['host'] + req.url });
    res.end();
}).listen(80);