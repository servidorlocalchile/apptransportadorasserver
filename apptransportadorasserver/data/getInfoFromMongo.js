require('dotenv').config();
const mongoose = require('mongoose');
const schemas = require('../schemas')
const uri = process.env.MONGOURL;

//connection to MongoDB
mongoose.Promise = global.Promise;
mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true,  useFindAndModify: false  } );
const db = mongoose.connection;
const SignupInputModel = mongoose.model("Model", schemas.blogSchema, 'signupInputModel');

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
  console.log('estamos conectados a la base de datos');
});

const getDatabaseInfo = (email)=>{
    SignupInputModel.exists({ Email: email }, function(err, result) {
        if(err){console.log('hubo un error al conectarse la base de datos', err)}
        if(!result){
            console.log('el correo no existe en la DB');
        }
        if(result){
            console.log('el email si existe en la base de datos', email, result);
            SignupInputModel.findOne({ Email: email },  (err, doc)=>{
                if(err){console.log('nos conectamos a la db pero hubo un error al obtener el documento email de la coleccion');}
                else {
                    console.log('el documeneto es ', doc);
                }
            })
        }
    })
}
module.exports = {getDatabaseInfo}